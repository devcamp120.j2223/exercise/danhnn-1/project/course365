// import express
const express = require('express');

// khởi tạo thư viện mongoose
const mongoose = require('mongoose');

// import router
const courseRouter = require('./app/router/courseRouter')

// khai báo thư viện path
const path = require('path');

// khởi tạo app express
const app = express();

// hiển thị hình ảnh
app.use(express.static(__dirname + '/view'))

// khai báo middleware đọc được json
app.use(express.json());
// khai báo middleware đọc được dữ liệu utf-8
app.use(express.urlencoded({
    extended: true
}))

// khởi tạo cổng project
const port = 8000;
// kết nối mongoDB
mongoose.connect("mongodb://localhost:27017/CRUD_Course365", function(error) {
    if (error) throw error;
    console.log('Successfully connected');
   })
   

// khởi api dạng get trả về giao diện course
app.get("/", (request, response) => {
    console.log(__dirname);
    response.sendFile(path.join(__dirname + "/view/index.html"))
})

// chạy router
app.use("/", courseRouter)

// chạy app express
app.listen(port, () => {
    console.log("app listening on port (ứng dụng đang chạy trên cổng)" + port)
})