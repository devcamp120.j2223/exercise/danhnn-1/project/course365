// khởi tạo thư viện express
const express = require('express');

const { getAllCourse, createCourse, getCourseById, updateCourseById, deleteCourseById } = require('../controller/courseController')

// khởi tạo router
const router = express.Router();

router.get("/course", getAllCourse);

router.post("/course", createCourse);

router.get("/course/:courseId", getCourseById);

router.put("/course/:courseId", updateCourseById);

router.delete("/course/:courseId", deleteCourseById);

// export dữ liệu thành 1 modules
module.exports = router;

