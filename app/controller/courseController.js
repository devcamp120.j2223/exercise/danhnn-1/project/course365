// import course model
const courseModel = require('../model/courseModel');
// import mongoose
const mongoose = require('mongoose');

// tạo course
const createCourse = (request, response) => {
    //b1: thu thập dữ liệu
    let courseBody = request.body;
    //b2: validate dữ liệu
    if (!courseBody.courseCode) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "courseCode is require"
        })
    }

    if (!courseBody.courseName) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "courseName is require"
        })
    }

    if (!(Number.isInteger(courseBody.price) && courseBody.price > 0)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "price is not valid"
        })
    }

    if (!(Number.isInteger(courseBody.discountPrice) && courseBody.discountPrice >= 0)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "discountPrice is not valid"
        })
    }

    if (!courseBody.duration) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "duration is require"
        })
    }

    if (!courseBody.level) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "level is require"
        })
    }

    if (!courseBody.coverImage) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "coverImage is require"
        })
    }

    if (!courseBody.teacherName) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "teacherName is require"
        })
    }

    if (!courseBody.teacherPhoto) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "teacherPhoto is require"
        })
    }
    //b3: thao tác với cơ sở dữ liệu
    let createCourse = {
        _id: mongoose.Types.ObjectId(),
        courseCode: courseBody.courseCode,
        courseName: courseBody.courseName,
        price: courseBody.price,
        discountPrice: courseBody.discountPrice,
        duration: courseBody.duration,
        level: courseBody.level,
        coverImage: courseBody.coverImage,
        teacherName: courseBody.teacherName,
        teacherPhoto: courseBody.teacherPhoto,
        isPopular: courseBody.isPopular,
        isTrending: courseBody.isTrending,
    }
    courseModel.create(createCourse, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal sever Error",
                message: error.message
            })
        }
        else {
            response.status(201).json({
                status: "Success: Create course success",
                data: data
            })
        }
    })
}
const getAllCourse = (request, response) => {
    //b1: thu thập dữ liệu
    //b2: validate dữ liệu
    //b3: thao tác với cơ sở dữ liệu
    courseModel.find((error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal Error Sever",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Get all course",
                data: data
            })
        }
    })
}
const getCourseById = (request, response) => {
    //b1: thu thập dữ liệu
    let courseId = request.params.courseId;
    //b2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "Course Id is not valid"
        })
    }
    //b3: thao tác với cơ sở dữ liệu
    courseModel.findById(courseId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal Error Sever",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Get course by id",
                data: data
            })
        }
    })
}
const updateCourseById = (request, response) => {
    //b1: thu thập dữ liệu
    let courseId = request.params.courseId;
    let courseBody = request.body;
    //b2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "courseId is not valid"
        })
    }
    //b3: thao tác với cơ sở dữ liệu
    let createCourse = {
        courseCode: courseBody.courseCode,
        courseName: courseBody.courseName,
        price: courseBody.price,
        discountPrice: courseBody.discountPrice,
        duration: courseBody.duration,
        level: courseBody.level,
        coverImage: courseBody.coverImage,
        teacherName: courseBody.teacherName,
        teacherPhoto: courseBody.teacherPhoto,
        isPopular: courseBody.isPopular,
        isTrending: courseBody.isTrending,
    }
    courseModel.findByIdAndUpdate(courseId, courseBody, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal Error Sever",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Update course by id",
                data: data
            })
        }
    })
}
const deleteCourseById = (request, response) => {
    //b1: thu thập dữ liệu
    let courseId = request.params.courseId;
    //b2: validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        response.status(400).json({
            status: "Error 400: Bad request",
            message: "course id is not valid"
        })
    }
    //b3: thao tác với cơ sở dữ liệu
    courseModel.findByIdAndDelete(courseId, (error, data) => {
        if (error) {
            response.status(500).json({
                status: "Error 500: Internal Error Sever",
                message: error.message
            })
        } else {
            response.status(200).json({
                status: "Success: Delete course by id"
            })
        }
    })
}

// export controller thành 1 module là 1 object gồm các hàm
module.exports = {
    createCourse: createCourse,
    getAllCourse: getAllCourse,
    getCourseById: getCourseById,
    updateCourseById: updateCourseById,
    deleteCourseById: deleteCourseById,
}